﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcMusicStoreSPA.Models;

namespace MvcMusicStoreSPA.Controllers
{
    public class HomeController : Controller
    {
	    private MusicStoreEntities _entities;

		public HomeController()
		{
			this._entities = new MusicStoreEntities();
		}

        public ActionResult Index()
        {
	        var model = this._entities.Artists.ToList();
			
			return View(model);
        }

    }
}
